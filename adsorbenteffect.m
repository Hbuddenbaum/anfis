function [ff,avMAET,avRMSET,avR2T,avMAETT,avRMSETT,avR2TT,stMAET,stRMSET,stR2T,stMAETT,stRMSETT,stR2TT]=adsorbenteffect(a,A,pin)
% function adsorbenteffect
% [ff,avMAET,avRMSET,avR2T,avMAETT,avRMSETT,avR2TT,stMAET,stRMSET,...
%    stR2T,stMAETT,stRMSETT,stR2TT]=adsorbenteffect(a,A,pin);
% See also
% B. Agbaogun, J. Alonso Moral, H. Buddenbaum & K. Fischer (2021):
% "Modelling of the adsorption of urea herbicides by tropical soils
% with an Adaptive-Neural-based Fuzzy Inference System",
% Journal of Chemometrics, 2021.
ff=[];
maeT=[];
rmseT=[];
r2T=[];
maeTT=[];
rmseTT=[];
r2TT=[];
for n=0:9
   dtrain=load([pin sprintf('ads-%s.txt.aux.train.%d.txt',a,n)]);
   dtest=load([pin sprintf('ads-%s.txt.aux.test.%d.txt',a,n)]);
%    [fis,maeTrain,rmseTrain,maeTest,rmseTest,r2Test]=fisgenplot_test(dtrain,dtest,n,a,A); 
   [fis,maeTrain,rmseTrain,r2Train,maeTest,rmseTest,r2Test]=fisgenplot_test(dtrain,dtest,n,a,A);
%    fprintf('FOLD=%d \n',n);
%    fprintf('   MAE(TRAIN)= %.2f and RMSE(TRAIN)=%.2f \n',maeTrain,rmseTrain);
%    fprintf('   MAE(TEST)= %.2f and RMSE(TEST)=%.2f \n',maeTest,rmseTest);
   maeT=[maeT maeTrain];
   rmseT=[rmseT rmseTrain];
   r2T=[r2T r2Train];
   maeTT=[maeTT maeTest];
   rmseTT=[rmseTT rmseTest];
   r2TT = [r2TT r2Test];
   ff=[ff fis];
end
avMAET=mean(maeT);
stMAET=std(maeT);
avRMSET=mean(rmseT);
stRMSET=std(rmseT);
avR2T=mean(r2T);
stR2T=std(r2T);
avMAETT=mean(maeTT);
stMAETT=std(maeTT);
avRMSETT=mean(rmseTT);
stRMSETT=std(rmseTT);
avR2TT=mean(r2TT);
stR2TT=std(r2TT);
% fprintf('\n');
% fprintf('Average (10 folds)\n');
% fprintf('   MAE(TRAIN)= %.2f and RMSE(TRAIN)=%.2f \n',avMAET,avRMSET);
% fprintf('   MAE(TEST)= %.2f and RMSE(TEST)=%.2f \n',avMAETT,avRMSETT);
% fprintf('Standard Deviation (10 folds)\n');
% fprintf('   MAE(TRAIN)= %.2f and RMSE(TRAIN)=%.2f \n',stMAET,stRMSET);
% fprintf('   MAE(TEST)= %.2f and RMSE(TEST)=%.2f \n',stMAETT,stRMSETT);
% fprintf('\n');
