%% Run Anfis for all variable sets
% See also
% B. Agbaogun, J. Alonso Moral, H. Buddenbaum & K. Fischer (2021):
% "Modelling of the adsorption of urea herbicides by tropical soils
% with an Adaptive-Neural-based Fuzzy Inference System",
% Journal of Chemometrics, 2021.

close all; clear; fclose all; format compact; clc
drive = 'E:';
basedir = [drive '\bitbucket\'];
cd(basedir)
cases = ['QE';'Kd']; % Two data sets

% Create Variable sets
for a=1:8
    per = perms(1:8);
    per = per(:,1:a);
    per = sort(per, 2);
    varsets{a} = unique(per, 'rows');
end

% Run for QE and/or Kd
for cas = 1:2
    c=0;
    pout = [basedir 'Figures_' cases(cas,:) '\'];
    if ~exist(pout, 'dir'), mkdir(pout); end
    for a=1:length(varsets)
        for b = 1:size(varsets{a},1)
            c=c+1;
            A = varsets{a}(b,:);
        
            adsorption_QE_Kd; % Call the Anfis wrapper
            
            % Draw figures and save them
            drawnow 
            print([pout  'Pred_Exp_Test_' num2str(length(A)) 'v ' num2str(A)],'-dpng')
            close
            print([pout  'Pred_Exp_Train_' num2str(length(A)) 'v ' num2str(A)],'-dpng')
            close
            disp([datestr(now) ', ' cases(cas,:) ', ' num2str(c)]);
            
            % Save results in Excel file
%             xlsout = [basedir 'Anfis_4'];
%             xlswrite(xlsout,[avMAET,avRMSET,avR2T,avMAETT,avRMSETT,avR2TT,...
%                 stMAET,stRMSET,stR2T,stMAETT,stRMSETT,stR2TT],...
%                 cases(cas,:),  ['a' num2str(c)])
%             xlswrite(xlsout, {VR}, cases(cas,:),  ['m' num2str(c)])            
        end
    end
end
