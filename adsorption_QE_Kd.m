% adsorption_QE_Kd
% Calls function adsorbenteffect and plots the results
% See also
% B. Agbaogun, J. Alonso Moral, H. Buddenbaum & K. Fischer (2021):
% "Modelling of the adsorption of urea herbicides by tropical soils
% with an Adaptive-Neural-based Fuzzy Inference System",
% Journal of Chemometrics, 2021.

% data for 10-fold CV
if cas ==1
    pin = [basedir 'Qe\'];
    d=load([pin 'ads-organics-Qe.txt.aux']);
    [ff,avMAET,avRMSET,avR2T,avMAETT,avRMSETT,avR2TT,stMAET,stRMSET,stR2T,stMAETT,stRMSETT,stR2TT]=adsorbenteffect('organics-QE',A,pin);
    plottitle = 'Q_e';%'Adsorbed Quantity';
    xini=-20;
    xend=140;
else
    pin = [basedir 'Kd\'];
    d=load([pin 'ads-organics-Kd.txt.aux']);
    [ff,avMAET,avRMSET,avR2T,avMAETT,avRMSETT,avR2TT,stMAET,stRMSET,stR2T,stMAETT,stRMSETT,stR2TT]=adsorbenteffect('organics-Kd',A,pin);
    plottitle = 'Kd';%'Partition Coefficient';
    xini=-10;
    xend=80;
end

i=d(:,A); 
o=d(:,9);
dt=[i o];
[fis,ERROR,STEPSIZE]=anfis(dt);
lim=length(o);
t=[1:lim];
anfisTrain= evalfis(fis,i);
anfisTest_= evalfis(ff,i);
for aa = 1:10
    anfisTest__(:,aa)= evalfis(ff(aa),i);
    anfisTest___((aa-1)*53+1:min([aa*53, 528])) = anfisTest__((aa-1)*53+1:min([aa*53, 528]));
end

err=abs(o-anfisTrain);
maeTrain=mean(err);
maeStd=std(err);
me=max(err);
rmseTrain=sqrt(mean(err.^2));
R2 = corrcoef(o,anfisTrain); R2=R2(2,1)^2;

%% Scatterplot Train
figure;

x=xini:1:xend;
xu=(xini+maeTrain+maeStd):1:(xend+maeTrain+maeStd);
xl=(xini-maeTrain-maeStd):1:(xend-maeTrain-maeStd);
pband=(95*me)/100;
xbu=(xini+pband):1:(xend+pband);
xbl=(xini-pband):1:(xend-pband);
plot(o,anfisTrain,'ob',x,x,'-k',x,xu,'-g',x,xl,'-g',x,xbu,'-r',x,xbl,'-r');
title(['Anfis Predicted vs Experimental ' plottitle]);
ylabel([plottitle ' (Predicted)'],'fontweight','bold');
xlabel([plottitle ' (Experimental)'],'fontweight','bold');
legend({'data points','y=x','margin: x+mae+std','margin: x-mae-std','margin: 95% of max error'},'location','Northwest');
VARNAMES = {'Co','Corg','CECeff','Feo','Mno','sicl','LogKow','Mw'};
VR = [];
text(0.75, 0.05+length(A)*0.045, 'Variables used:','sc','fontweight','bold')
for vn = 1:length(A)
    text(0.75, 0.05-vn*0.045+length(A)*0.045, VARNAMES{A(vn)}, 'sc')
    VR = [VR '_' VARNAMES{A(vn)}];
end

text(0.03, 0.68, ['R^2_{Train} = ' num2str(avR2T,3)], 'sc')
text(0.03, 0.61, ['RMSE_{Train} = ' num2str(avRMSET,3)], 'sc')
text(0.03, 0.54, ['MAE_{Train} = ' num2str(avMAET,3)], 'sc')
set(gca, 'linewidth', 0.75),
axis([xini xend xini xend])


%% Second Scatterplot 
figure;

x=xini:1:xend;
xu=(xini+maeTrain+maeStd):1:(xend+maeTrain+maeStd);
xl=(xini-maeTrain-maeStd):1:(xend-maeTrain-maeStd);
pband=(95*me)/100;
xbu=(xini+pband):1:(xend+pband);
xbl=(xini-pband):1:(xend-pband);
plot(o,anfisTest___,'ob',x,x,'-k',x,xu,'-g',x,xl,'-g',x,xbu,'-r',x,xbl,'-r');
title(['ANFIS Predicted vs Experimental ' plottitle]);
ylabel([plottitle ' (Predicted)'],'fontweight','bold');
xlabel([plottitle ' (Experimental)'],'fontweight','bold');
legend({'data points','y=x','margin: x+mae+std','margin: x-mae-std','margin: 95% of max error'},'location','Northwest');
VARNAMES = {'Co','Corg','CECeff','Feo','Mno','sicl','LogKow','Mw'};
VR = [];
text(0.75, 0.05+length(A)*0.045, 'Variables used:','sc','fontweight','bold')
for vn = 1:length(A)
    text(0.75, 0.05-vn*0.045+length(A)*0.045, VARNAMES{A(vn)}, 'sc')
    VR = [VR '_' VARNAMES{A(vn)}];
end

text(0.03, 0.68, ['R^2_{Test} = ' num2str(avR2TT,3)], 'sc')
text(0.03, 0.61, ['RMSE_{Test} = ' num2str(avRMSETT,3)], 'sc')
text(0.03, 0.54, ['MAE_{Test} = ' num2str(avMAETT,3)], 'sc')
set(gca, 'linewidth', 0.75),
axis([xini xend xini xend])