Readme to ANFIS project in Bitbucket

The MATLAB code provided here runs the analyses described in the publication "Modelling of the adsorption of urea herbicides by tropical soils with an Adaptive-Neural-based Fuzzy Inference System" by Babatunde Agbaogun, Josemaria Alonso Moral, Henning Buddenbaum and Klaus Fischer, Journal of Chemometrics, 2021.
The file "anfis_run_all" runs all analyses, using the data provided in the Downloads section of this repository. Please unzip Qe.zip and Kd.zip and place the directories "Kd" and/or "Qe" in the same folder as the MATLAB files.
Please specify the folder in lines 3 and 4 of "anfis_run_all".