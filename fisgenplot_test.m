function [fis,maeTrain,rmseTrain,r2Train,maeTest,rmseTest,r2Test]=fisgenplot_test(dtrain,dtest,f,a,A)
% function fisgenplot_test
% See also
% B. Agbaogun, J. Alonso Moral, H. Buddenbaum & K. Fischer (2021):
% "Modelling of the adsorption of urea herbicides by tropical soils
% with an Adaptive-Neural-based Fuzzy Inference System",
% Journal of Chemometrics, 2021.

opt = anfisOptions('DisplayANFISInformation', 0, 'DisplayFinalResults', 0, 'DisplayStepSize', 0);
opt.DisplayErrorValues = 0;
opt.DisplayStepSize = 0;

i=dtrain(:,A);
o=dtrain(:,9);
dt=[i o];
fis=anfis(dt, opt); %%%%%%%%%%%% ANFIS
lim=length(o);
t=[1:lim];
anfisTrain= evalfis(fis,i);%%%%%%%%%%%%%% anfisTrain= evalfis(i,fis);
%figure;
%plot(t',o,'ob-',t',anfisTrain,'*g-');
%title(sprintf('Anfis Prediction (TRAIN, fold=%d) - %s',f,a));
%legend('real output','estimated output');
err=abs(o-anfisTrain);
maeTrain=mean(err);
rmseTrain=sqrt(mean(err.^2));
r2Train = corrcoef(o,anfisTrain); r2Train=r2Train(2,1)^2;
it=dtest(:,A);
ot=dtest(:,9);
anfisTest= evalfis(fis,it); %%%%%%%%%%%%%%%%%%%%%%%% anfisTest= evalfis(it,fis);
limt=length(ot);
tt=[1:limt];
%figure;
%plot(tt',ot,'ob-',tt',anfisTest,'*g-');
%title(sprintf('Anfis Prediction (TEST, fold=%d) - %s',f,a));
%legend('real output','estimated output');
errTest=abs(ot-anfisTest);
maeTest=mean(errTest);
rmseTest=sqrt(mean(errTest.^2));
r2Test = corrcoef(ot,anfisTest); r2Test=r2Test(2,1)^2;
